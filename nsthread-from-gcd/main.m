//
//  main.m
//  Multithreading
//
//  Created by Alexey Ulenkov on 18.05.17.
//  Copyright © 2017 Alexey Ulenkov. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SUCCESS 0
static BOOL const debugPrint = YES;

// Параллельный обход массива и подсчет суммы
static NSArray<NSNumber *> *collection;
static NSUInteger maxThreadCount;

NSCondition *condVar;

//NSLock *lock;

// Task struct
typedef struct Task {
    NSRange range; // Рейнж части исходной коллекции которую будем обрабатывать
    NSUInteger threadID;
    bool finished; // Признак окончания таска (используется для возврата значения)
    NSUInteger sum; // Сумма элементов возвращаемой части коллекции (используется для возврата значения)
} Task;

void* threadEnumerateArray(void *);
bool checkCondition(Task **);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Получаем количество активных процессоров, столько потоков и будем создавать
        maxThreadCount = 7; //[[NSProcessInfo processInfo] activeProcessorCount];
        
        // Создаем коллекцию для обработки
        NSArray *templateCollection = @[@(1),@(2),@(3),@(4),@(5),@(6),@(7),@(8),@(9),@(10),@(11),@(12),@(13),@(14),@(15)];
        NSUInteger multiplier = 1000*1000; //000000;
        NSMutableArray *tmp = [[NSMutableArray alloc] initWithCapacity:templateCollection.count*multiplier];
        for (NSUInteger i=0; i<multiplier; ++i) {
            [tmp addObjectsFromArray:templateCollection];
        }
        collection = [tmp copy];
        
        Task* allThreadArgs[maxThreadCount]; // Массив структур
        
//        lock = [NSLock new];
        condVar = [NSCondition new];

        NSTimeInterval startTime; // Измерение производительности
        NSTimeInterval endTime;
        
        startTime = [NSDate new].timeIntervalSince1970;
        
        // Рассчитываем рейнжи для частей массива и запускаем обработку
        NSUInteger addition = collection.count%maxThreadCount;
        // Fork
        for (NSUInteger i=0; i<maxThreadCount; ++i) {
            NSUInteger length = collection.count/maxThreadCount;
            NSUInteger step = collection.count/maxThreadCount;
            
            if ( addition!=0 && i==(maxThreadCount-1) ) {
                length += addition;
            }
            
            NSRange subarrayRange = NSMakeRange(i*step, length);
            NSThread *thread;
            
            if (debugPrint) { NSLog(@"2 got here"); }

            // Создаем и заполняем структуру Task, через которую будут переданы аргументы в поток
            Task *args = malloc(sizeof(Task));
            args->threadID = i;
            args->range = subarrayRange;
            args->finished = false;
            allThreadArgs[i] = args;
            
            // Создание потока
            thread = [[NSThread alloc] initWithBlock:^{
                threadEnumerateArray(args);
            }];
            // Запуск потока
            [thread start];
        }
        
        if (debugPrint) { NSLog(@"5 got here"); }
        // Реализация шаблона "Ожидание на условной переменной"
        [condVar lock];
        while (!checkCondition(allThreadArgs))
        {
            // Condvar waiting
            if (debugPrint) { NSLog(@"6 condvar wait start"); }
            [condVar wait];
            if (debugPrint) { NSLog(@"6 condvar wait finish"); }
        }
        [condVar unlock];
        
        // Расчет результата
        NSUInteger result = 0;
        for (NSUInteger i=0; i<maxThreadCount; ++i) {
            Task* args = allThreadArgs[i];
            result += args->sum;
        }
        NSLog(@"Fork-join result %lu", result);
        
        // Очистка памяти
        for (NSUInteger i=0; i<maxThreadCount; ++i) {
            Task* args = allThreadArgs[i];
            free(args);
            args = NULL;
        }
        
        endTime = [NSDate new].timeIntervalSince1970;
        NSLog(@"Fork-join measured time %f", endTime-startTime);
    }
    return 0;
}

bool checkCondition(Task **threadArguments) {
    if (debugPrint) { NSLog(@"4 got here"); };
    bool result = true;
    for (NSUInteger i=0; i<maxThreadCount; ++i) {
        result = result & threadArguments[i]->finished;
    }
    return result;
}

void* threadEnumerateArray(void *args) {
    if (debugPrint) { NSLog(@"3 got here"); };
    
    Task *arguments = (Task *)args;
    NSRange range = arguments->range;
    NSUInteger sum = 0;
    
    for (NSUInteger i=range.location; i<(range.location+range.length);++i) {
        sum = sum + collection[i].integerValue;
    }
    arguments->finished = true;
    arguments->sum = sum;
    
    // Реализация шаблона "Ожидание на условной переменной"
//    [lock lock];
    [condVar signal];
//    [lock unlock];
    
    return SUCCESS;
}
